REM Start of BASIC! Include Program CallFileBrowser.bas

FN.DEF GetOpenFilePath$(aFile$,multiple$)

 LIST.CREATE S, commandListPointer
 LIST.ADD commandListPointer~
 "new Intent(Intent.ACTION_GET_CONTENT);" ~
 "setPackage("+ CHR$(34) +"com.blackmoonit.android.FileBrowser"+ CHR$(34) +");" ~
 !"setData("+ CHR$(34) + aFile$ + chr$(34) +");" ~  %//default file / jump directly to this file/folder
 !"setDataAndType(" + CHR$(34) + aFile$ + chr$(34) + "," + CHR$(34) + "*/*" + CHR$(34) + ");" ~
 !"putExtra(Intent.EXTRA_TITLE,"+ CHR$(34) + customTitle$ + CHR$(34) +");" ~
 !"putExtra("+ CHR$(34) + "com.blackmoonit.extra.ALLOW_MULTIPLE" + CHR$(34) + ",true);" ~
 "putExtra("+ CHR$(34) + "com.blackmoonit.extra.ALLOW_MULTIPLE" + CHR$(34) + ","+multiple$+");" ~
 !
 ! Direct from developer.android.com. But this is available for Android API 18+ only.
 ! "putExtra("+ CHR$(34) + "Intent.EXTRA_ALLOW_MULTIPLE" + CHR$(34) + ",true);" ~
 ! "putExtra("+ CHR$(34) + "Intent.EXTRA_LOCAL_ONLY" + CHR$(34) + ",true);" ~
 ! if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
 !
 !"setType("+ CHR$(34) + "image/*"+ CHR$(34) + ");" ~
 !  "setType("+ CHR$(34) + "image/gif"+ CHR$(34) + ");" ~
 !  "setType("+ CHR$(34) + "text/html"+ CHR$(34) + ");" ~
 !"setType("+ CHR$(34) + "folder/*"+ CHR$(34) + ");" ~
 "setType("+ CHR$(34) + "*/*"+ CHR$(34) + ");" ~
 !"setType("+ CHR$(34) + "multipart/mixed"+ CHR$(34) + ");" ~
 "addCategory(Intent.CATEGORY_DEFAULT);" ~
 "addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);" ~
 "addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);" ~
 "addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);" ~
 "EOCL"

 LIST.CREATE S, resultListPointer
 IF multiple$ = "true"
  LIST.ADD resultListPointer~
  "listS#theFileUriList=getParcelableArrayListExtra(Intent.EXTRA_STREAM);" ~
  "stackS#theFileUriList=getParcelableArrayListExtra(Intent.EXTRA_STREAM);" ~
  "theFileUriList$[]=getParcelableArrayListExtra(Intent.EXTRA_STREAM);" ~
  "EORL"
 ELSE
  LIST.ADD resultListPointer~
  "theFilePath$=getData().getPath();" ~
  "EORL"
 ENDIF
 LIST.CREATE S, theFileUriListPointer
 STACK.CREATE S, theFileUriStackPointer
 DIM theFileUriList$[1]

 BUNDLE.CREATE appVarPointer
 BUNDLE.PL appVarPointer,"***CommandList***",commandListPointer
 BUNDLE.PL appVarPointer,"***ResultList***",resultListPointer
 BUNDLE.PUT appVarPointer,"theFilePath$","No file selected!"
 BUNDLE.PL appVarPointer,"listS#theFileUriList",theFileUriListPointer
 BUNDLE.PS appVarPointer,"stackS#theFileUriList",theFileUriStackPointer
 BUNDLE.PUT appVarPointer,"theFileUriList$[]",theFileUriList$[]

 !BUNDLE.PUT.BUNDLE appVarPointer,"BUNDLE",appBundlePointer
 !BUNDLE.PUT.LIST appVarPointer,"LIST",appListPointer
 !BUNDLE.PUT.Stack appVarPointer,"STACK",appStackPointer

 !BUNDLE.GET.BUNDLE appVarPointer,"BUNDLE",appBundlePointer2
 !BUNDLE.GET.LIST appVarPointer,"LIST",appListPointer2
 !BUNDLE.GET.Stack appVarPointer,"STACK",appStackPointer2


 APP.SAR appVarPointer


 BUNDLE.GET appVarPointer,"theFilePath$",theFilePath$
 IF multiple$ = "true"

  !BUNDLE.GL appVarPointer,"listS#theFileUriList",theFileUriListPointer %Unbedingt Fehler abfangen!!
  BUNDLE.GS appVarPointer,"stackS#theFileUriList",theFileUriStackPointer  %Unbedingt Fehler abfangen!!

  BUNDLE.GET appVarPointer,"theFileUriList$[]",theFileUriList2$[]
 !!
 List.size theFileUriListPointer, listSize
 PRINT "listSize: ", listSize
 FOR i = 1 TO listSize
	LIST.GET theFileUriListPointer, i, text$
	PRINT "From List: ",text$
 NEXT I
 !!



  STACK.ISEMPTY theFileUriStackPointer, stackIsE
  PRINT "stackIsE: ", stackIsE
  IF stackIsE < 1
   DO
    STACK.POP theFileUriStackPointer, stackText$
    STACK.ISEMPTY theFileUriStackPointer, stackIsE
    PRINT "stackText$: ", stackText$
   UNTIL stackIsE > 0
  ENDIF


  ARRAY.LENGTH arrayLength, theFileUriList2$[]
  FOR i = 1 TO arrayLength
   text$ = theFileUriList2$[i]
   PRINT "fromArray: ", text$
  NEXT I

  BUNDLE.KEYS appVarPointer, lp
  LIST.SIZE lp, listSize
  PRINT "listSize: ", listSize

  FOR i = 1 TO listSize
   LIST.GET lp, i, text$
   PRINT text$
  NEXT I

 ENDIF

 FN.RTN theFilePath$
FN.END
DEBUG.OFF
FILE.ROOT dataPath$

fileName$ = "file://" + dataPath$  % + "/"% + "De_Re_BASIC!.pdf"
PRINT fileName$

!!
PRINT "File path: " ; GetOpenFilePath$(fileName$, "false") %Works two times! Issue #79
!!


myFilePath$ = GetOpenFilePath$(fileName$, "false")
PRINT "File path: " ; myFilePath$

DIALOG.MESSAGE "", "Go on", buttonResult, "Go"
!	
myFilePath$ = GetOpenFilePath$(fileName$, "true")
PRINT "File path: " ; myFilePath$

END
